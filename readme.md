Install:

```bash
# SNIP_VERSION=v1.0.0
curl -s https://gitlab.com/ytopia/ops/snip/-/raw/master/install.sh | sudo bash

```

emoji render in terminal
```bash
sudo apt install ttf-mscorefonts-installer
```