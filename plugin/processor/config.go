package processor

import "gitlab.com/ytopia/ops/snip/variable"

type Config struct {
	RunVars variable.RunVars
}
